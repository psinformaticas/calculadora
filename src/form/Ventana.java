package form;

import java.awt.EventQueue;

import javax.swing.JFrame;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.math.BigDecimal;
import java.awt.event.ActionEvent;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

import control.Calculadora;

import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;
import javax.swing.JSeparator;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import java.awt.SystemColor;

public class Ventana {

	private JFrame frame;
	private Calculadora c;
	private JLabel linea1;
	private JLabel linea2;
	private JPanel visor;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ventana window = new Ventana();
					window.frame.setVisible(true);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Ventana() {
		initialize();
		c = new Calculadora(); 
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {

		frame = new JFrame();
		frame.getContentPane().setBackground(Color.BLACK);
		frame.setBounds(100, 100, 328, 559);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setLocationRelativeTo(null);
		
		JButton btn1 = new JButton("1");
		btn1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				accionBoton("1");
			}
		});
		btn1.setBounds(26, 362, 56, 42);
		frame.getContentPane().add(btn1);

		JButton btn2 = new JButton("2");
		btn2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				accionBoton("2");
			}
		});
		btn2.setBounds(94, 362, 56, 42);
		frame.getContentPane().add(btn2);
		
		JButton btn3 = new JButton("3");
		btn3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				accionBoton("3");
			}
		});
		btn3.setBounds(162, 362, 56, 42);
		frame.getContentPane().add(btn3);
		
		JButton btn4 = new JButton("4");
		btn4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				accionBoton("4");
			}
		});
		btn4.setBounds(26, 307, 56, 42);
		frame.getContentPane().add(btn4);
		
		JButton btn5 = new JButton("5");
		btn5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				accionBoton("5");
			}
		});
		btn5.setBounds(94, 307, 56, 42);
		frame.getContentPane().add(btn5);
		
		JButton btn6 = new JButton("6");
		btn6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				accionBoton("6");
			}
		});
		btn6.setBounds(162, 307, 56, 42);
		frame.getContentPane().add(btn6);
		
		JButton btn7 = new JButton("7");
		btn7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				accionBoton("7");
			}
		});
		btn7.setBounds(26, 252, 56, 42);
		frame.getContentPane().add(btn7);
		
		JButton btn8 = new JButton("8");
		btn8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				accionBoton("8");
			}
		});
		btn8.setBounds(94, 252, 56, 42);
		frame.getContentPane().add(btn8);
		
		JButton btn9 = new JButton("9");
		btn9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				accionBoton("9");
			}
		});
		btn9.setBounds(162, 252, 56, 42);
		frame.getContentPane().add(btn9);
		
		JButton btn0 = new JButton("0");
		btn0.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				accionBoton("0");
			}
		});
		btn0.setBounds(94, 417, 56, 42);
		frame.getContentPane().add(btn0);
		
		JButton btnDec = new JButton(",");
		btnDec.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				accionBoton(".");
			}
		});
		btnDec.setBounds(162, 417, 56, 42);
		frame.getContentPane().add(btnDec);
		
		JButton btnPower = new JButton("ON");
		btnPower.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (c.isEncendida()) {
					c.setEncendida(false);
					apagar();
					btnPower.setText("ON");
					btnPower.setForeground(new Color(0, 128, 0));
				} else {
					c.setEncendida(true);
					encender();
					limpiarCalc();
					btnPower.setText("OFF");
					btnPower.setForeground(new Color(255, 0, 0));
				}
			}
		});
		btnPower.setForeground(new Color(0, 128, 0));
		btnPower.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnPower.setBounds(26, 417, 56, 42);
		frame.getContentPane().add(btnPower);
		
		JButton btnCe = new JButton("CE");
		btnCe.setForeground(new Color(255, 0, 0));
		btnCe.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setVisor(c.fnCE());
			}
		});
		btnCe.setBounds(26, 197, 56, 42);
		frame.getContentPane().add(btnCe);
		
		JButton btnC = new JButton("C");
		btnC.setForeground(new Color(255, 0, 0));
		btnC.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				limpiarCalc();
			}
		});
		btnC.setBounds(94, 197, 56, 42);
		frame.getContentPane().add(btnC);
		
		JButton btnBorrar = new JButton("<x");
		btnBorrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setVisor(c.remNum());
			}
		});
		
		btnBorrar.setForeground(new Color(255, 0, 0));
		btnBorrar.setToolTipText("<x");
		btnBorrar.setBounds(162, 197, 56, 42);
		frame.getContentPane().add(btnBorrar);
		
		JButton btnDiv = new JButton("/");
		btnDiv.setForeground(new Color(0, 0, 255));
		btnDiv.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				c.division(linea1.getText());
			}
		});
		btnDiv.setToolTipText("/");
		btnDiv.setBounds(230, 197, 56, 42);
		frame.getContentPane().add(btnDiv);
		
		JButton btnMult = new JButton("X");
		btnMult.setForeground(new Color(0, 0, 255));
		btnMult.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				c.multiplicacion(linea1.getText());
			}
		});
		btnMult.setBounds(230, 252, 56, 42);
		frame.getContentPane().add(btnMult);
		
		JButton btnResta = new JButton("-");
		btnResta.setForeground(new Color(0, 0, 255));
		btnResta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				c.resta(linea1.getText());
			}
		});
		btnResta.setBounds(230, 307, 56, 42);
		frame.getContentPane().add(btnResta);
		
		JButton btnSuma = new JButton("+");
		btnSuma.setForeground(new Color(0, 0, 255));
		btnSuma.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (!c.isEncendida()) {
					c.suma(linea1.getText());
				} else {
					c.suma(linea1.getText());
				}
			}
		});
		btnSuma.setBounds(230, 362, 56, 42);
		frame.getContentPane().add(btnSuma);
		
		JButton btnIgual = new JButton("=");
		btnIgual.setForeground(new Color(0, 0, 255));
		btnIgual.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setVisor(""+c.getResultado(linea1.getText()));
				if (!linea1.getText().equals("ERROR")) {
					linea2.setText(c.getHist());	
				}
				
				c.setMuestraRes(true);
				
			}
		});
		btnIgual.setBounds(230, 417, 56, 42);
		frame.getContentPane().add(btnIgual);
		
		visor = new JPanel();
		visor.setBackground(new Color(112, 128, 144));
		visor.setBorder(new LineBorder(new Color(0, 0, 0)));
		visor.setBounds(26, 27, 260, 64);
		frame.getContentPane().add(visor);
		visor.setLayout(null);
		
		linea2 = new JLabel("0");
		linea2.setForeground(new Color(112, 128, 144));
		linea2.setFont(new Font("OCR A Extended", Font.PLAIN, 13));
		linea2.setHorizontalAlignment(SwingConstants.TRAILING);
		linea2.setBounds(12, 6, 236, 16);
		visor.add(linea2);
		
		linea1 = new JLabel("0");
		linea1.setForeground(new Color(112, 128, 144));
		linea1.setFont(new Font("OCR A Extended", Font.BOLD, 20));
		linea1.setHorizontalAlignment(SwingConstants.TRAILING);
		linea1.setBounds(12, 31, 236, 33);
		visor.add(linea1);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(26, 187, 260, 9);
		frame.getContentPane().add(separator);
		
		
		//BOTONES MEMORIA
		JButton btnMS = new JButton("MS");
		btnMS.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				c.setModoMemStorage(true);
				c.setHistorialAux(linea2.getText());
				linea2.setText("GUARDAR EN MEM - POSICI�N?");
			}
		});
		btnMS.setForeground(new Color(0, 128, 0));
		btnMS.setBounds(26, 142, 56, 42);
		frame.getContentPane().add(btnMS);
		
		JButton btnMR = new JButton("MR");
		btnMR.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				c.setModoMemRecall(true);
				c.setHistorialAux(linea2.getText());
				linea2.setText("LEER DE MEM - POSICI�N?");
			}
		});
		btnMR.setForeground(new Color(0, 128, 0));
		btnMR.setBounds(94, 142, 56, 42);
		frame.getContentPane().add(btnMR);
		
		JButton btnMC = new JButton("MC");
		btnMC.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				c.setModoMemRecall(false);
				c.setModoMemStorage(false);
				c.clearMem();
				c.setHistorialAux(linea2.getText());
				linea2.setText("MEMORIA VAC�A");
			}
		});
		btnMC.setForeground(new Color(0, 128, 0));
		btnMC.setBounds(162, 142, 56, 42);
		frame.getContentPane().add(btnMC);
		
		JButton btnCancelMem = new JButton("X");
		btnCancelMem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				c.setModoMemRecall(false);
				c.setModoMemStorage(false);
				linea2.setText(c.getHistorialAux());
			}
		});
		btnCancelMem.setForeground(Color.RED);
		btnCancelMem.setBounds(230, 142, 56, 42);
		frame.getContentPane().add(btnCancelMem);
		
		JButton btnDeshacer = new JButton("Deshacer");
		btnDeshacer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				c.deshacer();
				setVisor(Double.toString(c.getResultado()));
				linea2.setText(c.getHist());
			}
		});
		btnDeshacer.setBounds(162, 104, 124, 25);
		frame.getContentPane().add(btnDeshacer);
		
		JMenuBar menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);
		
		JMenu mnNewMenu = new JMenu("Ayuda");
		menuBar.add(mnNewMenu);
	}
	
	private void limpiarCalc() {
		linea1.setText("0");
		linea2.setText("0");
		c.inicializar();
		c.setMuestraRes(false);
	}
	
	private void accionBoton(String boton) {
		if (c.isModoMemStorage()) {
			c.setMem(Integer.parseInt(boton), getValorVisor());
			linea2.setText("ALMACENADO EN: M"+boton);
			c.setModoMemStorage(false);
		} else if (c.isModoMemRecall()) {
			linea1.setText(c.getMem(Integer.parseInt(boton)));
			linea2.setText("LE�DO DE: M"+boton);
			c.setModoMemRecall(false);
		} else {
			setVisor(c.addNum(boton));	
		}
	}
	private void setVisor(String n) {
		try {
			BigDecimal numero = new BigDecimal(n);  
			linea1.setText(numero.stripTrailingZeros().toPlainString());	
		} catch (Exception e) {
			linea1.setText("ERROR");
			linea2.setText(e.getClass().toString().replaceAll("class java.lang.", ""));
		}
		
	}
	
	private double getValorVisor() {
		return Double.parseDouble(linea1.getText());
	}


	//BOTONES ENCENDER APAGAR
	private void encender() {
		linea1.setForeground(new Color(0,0,0));
		linea2.setForeground(new Color(0,0,0));
		visor.setBackground(new Color(154, 205, 50));
	}
	
	private void apagar() {
		visor.setBackground(new Color(112, 128, 144));
		linea1.setForeground(new Color(112, 128, 144));
		linea2.setForeground(new Color(112, 128, 144));
	}
}
