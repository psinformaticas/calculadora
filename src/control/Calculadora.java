package control;

import java.math.BigDecimal;

import form.Ventana;

public class Calculadora {

	private StringBuilder valorSB, historial;
	private double resultado;
	private String operacion, historialAux;
	private double[] memoria;
	private boolean encendida, modoMemRecall, modoMemStorage, muestraRes;
	
	
	public Calculadora() {
		inicializar();
		this.memoria = new double[10];
		for (int x=0;x<memoria.length;x++) {
			memoria[x] = 0.0;
		}

		this.encendida = false;
		
	}
	
	public void inicializar() {
		this.valorSB = new StringBuilder();
		this.historial = new StringBuilder();
	    this.operacion = "";
	    this.historialAux = "";
		this.resultado = 0.0;
		this.modoMemRecall = false;
		this.modoMemStorage = false;
		this.muestraRes = false;
	}


	// GETTERS Y SETTERS
	public StringBuilder getValorSB() {
		return valorSB;
	}

	public void setValorSB(StringBuilder valorSB) {
		this.valorSB = valorSB;
	}
	
	
	public StringBuilder getHistorial() {
		return historial;
	}

	public void setHistorial(StringBuilder historial) {
		this.historial = historial;
	}

	public double getResultado() {
		return resultado;
	}

	public void setResultado(double resultado) {
		this.resultado = resultado;
	}

	public String getOperacion() {
		return operacion;
	}

	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	
	public double[] getMemoria() {
		return memoria;
	}

	public void setMemoria(double[] memoria) {
		this.memoria = memoria;
	}

	public boolean isEncendida() {
		return encendida;
	}

	public void setEncendida(boolean encendida) {
		this.encendida = encendida;
	}

	public boolean isModoMemRecall() {
		return modoMemRecall;
	}

	public void setModoMemRecall(boolean modoMemRecall) {
		this.modoMemRecall = modoMemRecall;
	}

	public boolean isModoMemStorage() {
		return modoMemStorage;
	}

	public void setModoMemStorage(boolean modoMemStorage) {
		this.modoMemStorage = modoMemStorage;
	}

	public boolean isMuestraRes() {
		return muestraRes;
	}

	public void setMuestraRes(boolean muestraRes) {
		this.muestraRes = muestraRes;
	}

	public String getHistorialAux() {
		return historialAux;
	}

	public void setHistorialAux(String historialAux) {
		this.historialAux = historialAux;
	}

	//FUNCIONES MEMORIA
	public String getMem(int pos) {
		BigDecimal numero = new BigDecimal(Double.toString(memoria[pos]));  
		return numero.stripTrailingZeros().toPlainString();
	}
	
	public double setMem(int pos, double n) {
		return memoria[pos] = n;
	}
	
	public void clearMem() {
		for (int x=0;x<memoria.length;x++) {
			memoria[x] = 0.0;
		}
	}
	
	//FUNCIONES CALCULO
	public void addHist(String h) {
		if (muestraRes == false) {
			if (historial.length() == 0) {
				historial.append(h);
			} else {
				historial.append(" "+operacion+" "+h);
			}
		
		}
		
	}
	
	public String getHist() {
		return historial.toString();
	}
	
	public String addNum(String n) {
		valorSB.append(n);
		//SE REVISA QUE NO TERMINE EN .
		if (valorSB.charAt(valorSB.length()-1) == '.') {
			valorSB.deleteCharAt(valorSB.length()-1);
		}
		
		return valorSB.toString();
	}
	
	public String remNum() {
		if (valorSB.length()>=2) {
			valorSB.deleteCharAt(valorSB.length()-1);	
		}
		return valorSB.toString();
	}
	
	public String fnCE() {
		valorSB = new StringBuilder("0");
		return valorSB.toString();
	}
	
	public void suma(String n) {
		operacion = "+";
		addHist(n);	
		resultado = Double.parseDouble(n);
		valorSB = new StringBuilder();
		muestraRes = false;
	}
	
	public void resta(String n) {
		operacion = "-";
		addHist(n);	
		resultado = Double.parseDouble(n);
		valorSB = new StringBuilder();
		muestraRes = false;
	}
	
	public void multiplicacion(String n) {
		operacion = "*";
		addHist(n);
		resultado = Double.parseDouble(n);
		valorSB = new StringBuilder();
		muestraRes = false;
	}
	
	public void division(String n) {
		operacion = "/";
		addHist(n);
		resultado = Double.parseDouble(n);
		valorSB = new StringBuilder();
		muestraRes = false;
	}
	
	public double getResultado(String n) {
		switch (operacion) {
			case "+":
				resultado = resultado + Double.parseDouble(n);
				break;
			case "-":
				resultado = resultado - Double.parseDouble(n);
				break;
			case "/":
				resultado = resultado / Double.parseDouble(n);
				break;
			case "*":
				resultado = resultado * Double.parseDouble(n);
				break;
		}
		addHist(n);
		muestraRes = true;
		operacion = "";
		
		
		return resultado;
	}

	//FUNCIONES DESHACER
	public void deshacer() {
		//SEPARA N�MEROS Y OP. EN ARRAY DE STRINGS
		String[] arr= historial.toString().split(" ");
		
		if (arr.length>2) {
			//VAC�A HISTORIAL
			historial = new StringBuilder("");
			
			//AGREGA AL HISTORIAL TODO MENOS �LTIMO N�M Y OP.
			
			for (int i = 0; i<arr.length-2; i++) {
				historial.append(arr[i]+" ");
			}
				
	
			double calc = Double.parseDouble(arr[0]);
			
			for (int i = 2; i<arr.length-2; i+=2) {
				
				switch (arr[i-1]) {
				case "+":
					calc = calc + Double.parseDouble(arr[i]);
					break;
				case "-":
					calc = calc - Double.parseDouble(arr[i]);
					break;
				case "/":
					calc = calc / Double.parseDouble(arr[i]);
					break;
				case "*":
					calc = calc * Double.parseDouble(arr[i]);
					break;
				}
				
			}
			
			//ELIMINA ULTIMO ESPACIO
			historial.deleteCharAt(historial.lastIndexOf(" "));
			
			resultado = calc;
		}
		
	}
	
	
}
