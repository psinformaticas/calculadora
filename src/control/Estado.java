package control;

public class Estado {
	private StringBuilder valorSB, historial;
	private double resultado;
	private String operacion, historialAux;
	private double[] memoria;
	private boolean encendida, modoMemRecall, modoMemStorage, muestraRes;

	public Estado() {
		this.valorSB = new StringBuilder();
		this.historial = new StringBuilder();
		this.resultado = 0.0;
		this.operacion = "";
		this.historialAux = "";
		this.memoria = new double[10];
		this.encendida = true;
		this.modoMemRecall = false;
		this.modoMemStorage = false;
		this.muestraRes = false;
	}
	

	public void guardarEstado(Calculadora c) {
		valorSB = c.getValorSB();
		historial = c.getHistorial();
		resultado = c.getResultado();
		operacion = c.getOperacion();
		historialAux = c.getHistorialAux();
		memoria = c.getMemoria();
		encendida = c.isEncendida();
		modoMemRecall = c.isModoMemRecall();
		modoMemStorage = c.isModoMemStorage();
		muestraRes = c.isMuestraRes();
	}
	
	public void leerEstado(Calculadora c) {
		c.setValorSB(valorSB);
		c.setHistorial(historial);
		c.setResultado(resultado);
		c.setOperacion(operacion);
		c.setHistorialAux(historialAux);
		c.setMemoria(memoria);
		c.setEncendida(encendida);
		c.setModoMemRecall(modoMemRecall);
		c.setModoMemStorage(modoMemStorage);
		c.setMuestraRes(muestraRes);
	}
}
